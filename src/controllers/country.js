import axios from 'axios';
import Promise from 'bluebird';
import { sendResponse, sendError, ResponseError } from '../util/response';


const RESTCOUNTRIES_API_URL = 'https://restcountries.eu/rest/v2';

const getCountries = async (name, fullText = true) => {
  console.debug('getCountry');

  const API_URL = `${RESTCOUNTRIES_API_URL}/name`;

  if (!name) {
    throw new ResponseError({ status: 422 });
  }

  return axios.get(`${API_URL}/${name}?fullText=${fullText}`);
};

export const getCountryByName = async (req, res) => {
  console.debug('getCountryByName');
  try {
    const { name } = req.body;
    const ret = await getCountries(name, true);
    sendResponse(res, { data: ret.data[0] });
  } catch (err) {
    sendError(res, err);
  }
};

export const getCountriesByName = async (req, res) => {
  console.debug('getCountriesByName');
  try {
    const { name } = req.body;
    const ret = await getCountries(name, false);
    sendResponse(res, { data: ret.data });
  } catch (err) {
    sendError(res, err);
  }
};

export const getManyCountries = async (req, res) => {
  console.debug('getCountries');

  const API_URL = `${RESTCOUNTRIES_API_URL}/name`;

  try {
    const { names } = req.body;

    if (!names) {
      throw new ResponseError({ status: 422 });
    }

    let countries = [];

    Promise.map(names, async name => {
      await axios.get(`${API_URL}/${name}`)
        .then(response => {
          countries = countries.concat(response.data);
        })
        .catch(error => {
          console.log(error);
          sendError(res, error);
        });
    }).then(() => {
      sendResponse(res, { data: countries });
    });
  } catch (err) {
    sendError(res, err);
  }
};

export const getAllCountries = async (req, res) => {
  console.debug('getAllCountries');

  const API_URL = `${RESTCOUNTRIES_API_URL}/all`;

  try {
    axios.get(API_URL)
      .then(response => {
        sendResponse(res, { data: response.data });
      })
      .catch(error => {
        console.log(error);
        sendError(res, error);
      });
  } catch (err) {
    sendError(res, err);
  }
};
