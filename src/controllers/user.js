import { v4 as uuidv4 } from 'uuid';
import jwt from 'jsonwebtoken';
import { sendResponse, sendError, ResponseError } from '../util/response';

const registeredUsers = [
  { id: uuidv4(), name: 'administrator', email: 'admin@mail.com', password: '123456', immutable: true }
];

export const login = async (req, res) => {
  console.debug('login');

  try {
    const { email, password } = req.body;

    if (!email && !password) {
      throw new ResponseError({ status: 422 });
    }

    const user = registeredUsers.find(u => u.email === email && u.password === password);

    if (user) {
      const token = jwt.sign({ id: user.id }, process.env.SECRET, {
        expiresIn: 300 // expires in 5min
      });
      sendResponse(res, {
        data: {
          id: user.id, name: user.name, email: user.email, token
        }
      });
    } else {
      sendError(res, { status: 401, message: 'not authorized' });
    }
  } catch (err) {
    sendError(res, err);
  }
};

export const save = async (req, res) => {
  console.debug('save user');

  try {
    const { name, email, password } = req.body;

    if (!name || !email || !password) {
      throw new ResponseError({ status: 422 });
    }

    const user = req.body;
    user.id = uuidv4();

    registeredUsers.push(user);

    sendResponse(res);
  } catch (err) {
    sendError(res, err);
  }
};

export const list = async (req, res) => {
  console.debug('list users');
  try {
    const users = registeredUsers.map(u => ({ id: u.id, name: u.name, email: u.email, immutable: u.immutable }));
    sendResponse(res, { data: users });
  } catch (err) {
    sendError(res, err);
  }
};
