import bodyParser from 'body-parser';
import { getCountryByName, getManyCountries, getAllCountries, getCountriesByName } from './controllers/country';
import { login, list as listUsers, save } from './controllers/user';

export default app => {
  app.get('/', (req, res) => res.status(200).send('Hello, Backend service test!!'));

  app.post('/api/country/consultOne', bodyParser.json(), getCountryByName);
  app.post('/api/country/consult', bodyParser.json(), getCountriesByName);
  app.post('/api/country/consultMany', bodyParser.json(), getManyCountries);
  app.get('/api/country/all', getAllCountries);
  app.post('/api/user/login', bodyParser.json(), login);
  app.get('/api/user/all', listUsers);
  app.post('/api/user', bodyParser.json(), save);
};
