require('core-js/stable');
require('regenerator-runtime/runtime');
require('dotenv').config();

// eslint-disable-next-line import/newline-after-import
global.Promise = require('bluebird');
Promise.coroutine.addYieldHandler(value => Promise.resolve(value));

// Babel hook to transpile on-the-fly
if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line global-require
  require('@babel/register'); // eslint-disable-line import/no-extraneous-dependencies
}

// App
require('./app');
