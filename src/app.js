import express from 'express';
import http from 'http';
import cors from 'cors';
import router from './router';

let server = null;

const initServer = async () => {
  try {
    // Server setup
    const app = express();
    app.use(cors());
    const PORT = 3001;
    const HOSTNAME = '0.0.0.0';
    const APPNAME = 'backend test application';

    server = http.createServer(app);
    server.close = Promise.promisify(server.close);

    router(app);

    server.listen(PORT, HOSTNAME, () => console.log(`${APPNAME} STARTED on ${HOSTNAME}:${PORT}`));
  } catch (err) {
    console.error(err);
    process.kill(process.pid, 'SIGINT');
  }
};

initServer();

// Graceful shutdown

const shutdown = async type => {
  try {
    console.warn(`Receive ${type} signal`);

    console.warn('Graceful EXIT');
  } catch (err) {
    console.error(err);
  } finally {
    setTimeout(() => process.exit(), 3000); // Delay to finish log
  }
};

process.on('SIGINT', () => shutdown('SIGINT'));
process.on('SIGTERM', () => shutdown('SIGTERM'));
